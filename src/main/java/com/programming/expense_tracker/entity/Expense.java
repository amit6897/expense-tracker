package com.programming.expense_tracker.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_expenses")
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "expense_name")
    @NotNull(message = "expense name must not be null")
    private String name;

    private String description;

    @Column(name = "expense_amount")
    private BigDecimal amount;

    private String category;

    private Date date;

    @Column(name = "created_at", nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Timestamp updatedAt;
}


/**
 * CREATE database expensetracker;
 *
 * USE expensetracker;
 *
 * CREATE TABLE tbl_expenses
 * (
 * 	id INT PRIMARY KEY AUTO_INCREMENT,
 *     expense_name VARCHAR(255) NOT NULL,
 *     description VARCHAR(255) NOT NULL,
 *     expense_amount DOUBLE(5, 2) NOT NULL,
 *     category VARCHAR(255) NOT NULL,
 *     date DATE NOT NULL
 * );
 *
 * INSERT INTO tbl_expenses(expense_name, description, expense_amount, category, date)
 * VALUES("Water bill", "water bill", 600.00, "Bills", "2021-10-14");
 *
 * INSERT INTO tbl_expenses(expense_name, description, expense_amount, category, date)
 * VALUES("Electricity bill", "electricity bill", 900.00, "Bills", "2021-10-13");
 */
