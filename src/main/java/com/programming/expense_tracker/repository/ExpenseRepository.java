package com.programming.expense_tracker.repository;

import com.programming.expense_tracker.entity.Expense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    // SELECT * from tbl_expenses WHERE category=?
    Page<Expense> findByCategory(String category, Pageable page);

    // SELECT * from tbl_expenses WHERE name LIKE '%keyword%'
    Page<Expense> findByNameContaining(String keyword, Pageable page);

    // SELECT * from tbl_expenses WHERE date BETWEEN 'startDate' AND 'endDate'
    Page<Expense> findByDateBetween(Date startDate, Date endDate, Pageable page);
}
