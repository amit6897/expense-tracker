package com.programming.expense_tracker.service;

import com.programming.expense_tracker.entity.User;
import com.programming.expense_tracker.entity.UserModel;

public interface UserService {
    User createUser(UserModel user);
    User readUser(Long id);
    User updateUser(UserModel user, Long id);
    void deleteUser(Long id);
}
